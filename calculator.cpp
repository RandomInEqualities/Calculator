#include "calculator.h"

// Silence multiple defined copy constructor in x3::variant.
// Please remove this when it is fixed in boost.
#if defined(_MSC_VER)
#pragma warning(disable : 4521)
#endif

#include <boost/fusion/include/adapt_struct.hpp>
#include <boost/fusion/include/std_pair.hpp>
#include <boost/optional.hpp>
#include <boost/spirit/home/x3.hpp>
#include <boost/spirit/home/x3/support/ast/variant.hpp>
#include <boost/variant/apply_visitor.hpp>
#include <boost/variant/static_visitor.hpp>

#include <algorithm>
#include <cctype>
#include <cmath>
#include <functional>
#include <iostream>
#include <map>
#include <ostream>
#include <stdexcept>
#include <string>
#include <utility>
#include <vector>

#include <assert.h>

using namespace std::literals;

namespace x3 = boost::spirit::x3;

constexpr double pi = 3.14159265358979323846;
constexpr double euler = 2.71828182845904523536;


namespace ast {


using ExpressionTypes = x3::variant<
    double,
    x3::forward_ast<struct UnaryExpression>,
    x3::forward_ast<struct BinaryExpression>,
    x3::forward_ast<struct ExponentationExpression>,
    x3::forward_ast<struct Function>,
    x3::forward_ast<struct Variable>
>;


struct Expression : ExpressionTypes
{
    // Translated from a grammer expression match of the form:
    //
    // double | UnaryExpression | BinaryExpression | Function | Variable
    //
    using base_type::base_type;
    using base_type::operator=;
};


struct BinaryOperation
{
    // Translated from a grammer expression match of the form:
    //
    // COMMAND operand
    //
    // Where command can be [+,-,*,/].
    char command;
    Expression operand;
};


struct BinaryExpression
{
    // Translated from a grammer expression match of the form:
    //
    // operand (COMMAND operand)*
    //
    // Where command can be [+,-,*,/].
    Expression first;
    std::vector<BinaryOperation> others;
};


struct UnaryExpression
{
    // Translated from a grammer expression match of the form:
    //
    // [COMMAND] operand
    //
    // Where command can be [+,-].
    boost::optional<char> command;
    Expression operand;
};


struct ExponentationExpression {
    // Translated from a grammer expression match of the form:
    //
    // signed_atom (POW_COMMAND signed_atom)*
    UnaryExpression first;
    std::vector<UnaryExpression> others;
};


struct Function
{
    // Translated from a grammer expression match of the form:
    //
    // name LEFT_PARENTHESIS arguments* RIGHT_PARENTHESIS
    //
    std::string name;
    std::vector<Expression> arguments;
};


struct Variable
{
    // Translated from a grammer expression match of the form:
    //
    // name
    //
    std::string name;
};


} // namespace ast


/*
 * Create boost fusion tuples for the above structs. With these, spirit can
 * automaticly convert parsed expressions into structs.
 */
BOOST_FUSION_ADAPT_STRUCT(
    ast::UnaryExpression,
    (boost::optional<char>, command),
    (ast::Expression, operand)
);

BOOST_FUSION_ADAPT_STRUCT(
    ast::BinaryOperation,
    (char, command),
    (ast::Expression, operand)
);

BOOST_FUSION_ADAPT_STRUCT(
    ast::BinaryExpression,
    (ast::Expression, first),
    (std::vector<ast::BinaryOperation>, others)
);

BOOST_FUSION_ADAPT_STRUCT(
    ast::ExponentationExpression,
    (ast::UnaryExpression, first),
    (std::vector<ast::UnaryExpression>, others)
);

BOOST_FUSION_ADAPT_STRUCT(
    ast::Function,
    (std::string, name),
    (std::vector<ast::Expression>, arguments)
);

BOOST_FUSION_ADAPT_STRUCT(
    ast::Variable,
    (std::string, name)
);


namespace parser {

/*
 * Define the type that parses real numbers in the calculator grammer.
 *
 * We do not use the default x3::double_ and its default policy cause that one
 * comsumes signs (+-), which breaks our grammer. And lets users type the names
 * inf and nan to represent infinity and not-a-number (NaN).
 */

template <typename T>
struct RealPolicy : x3::ureal_policies<T>
{
    template <typename Iterator>
    static bool parse_sign(Iterator& /*first*/, Iterator const& /*last*/)
    {
        return false;
    }

    template <typename Iterator, typename Attribute>
    static bool parse_nan(Iterator& /*first*/, Iterator const& /*last*/, Attribute& /*attr_*/)
    {
        return false;
    }

    template <typename Iterator, typename Attribute>
    static bool parse_inf(Iterator& /*first*/, Iterator const& /*last*/, Attribute& /*attr_*/)
    {
        return false;
    }
};
x3::real_parser<double, RealPolicy<double>> const number_without_sign = {};

/*
 * Define the terminal symbols in the calculator grammer.
 *
 * Defines all symbols with spirit's rule class. This gives us a overview of
 * the synthesised attribute values (we could have used automatic type deductions
 * for some of the terminals).
 */

x3::rule<class LEFT_PARANTHESIS, x3::unused_type> const LEFT_PARANTHESIS = "LEFT_PARANTHESIS";
auto const LEFT_PARANTHESIS_def = x3::lit('(');

x3::rule<class RIGHT_PARENTHESIS, x3::unused_type> const RIGHT_PARENTHESIS = "RIGHT_PARENTHESIS";
auto const RIGHT_PARENTHESIS_def = x3::lit(')');

x3::rule<class FUNC_ARG_SEPARATOR, x3::unused_type> const FUNC_ARG_SEPARATOR = "FUNC_ARG_SEPARATOR";
auto const FUNC_ARG_SEPARATOR_def = x3::lit(',');

x3::rule<class ADD_OPERATOR, char> const ADD_OPERATOR = "ADD_OPERATOR";
auto const ADD_OPERATOR_def = x3::char_('+') | x3::char_('-');

x3::rule<class MULT_OPERATOR, char> const MULT_OPERATOR = "MULT_OPERATOR";
auto const MULT_OPERATOR_def = x3::char_('*') | x3::char_('/') | x3::char_('%');

x3::rule<class POW_OPERATOR, x3::unused_type> const POW_OPERATOR = "POW_OPERATOR";
auto const POW_OPERATOR_def = x3::string("^") | x3::string("**");

BOOST_SPIRIT_DEFINE(
    LEFT_PARANTHESIS,
    RIGHT_PARENTHESIS,
    FUNC_ARG_SEPARATOR,
    ADD_OPERATOR,
    MULT_OPERATOR,
    POW_OPERATOR
);


/*
 * Define the non-terminal symbols in the calculator grammer.
 *
 * Uses the following PEG grammer:
 *
 * expr: add
 * add: mult (ADD_OPERATOR mult)*
 * mult: factor (MULT_OPERATOR factor)*
 * factor: signed_atom (POW_OPERATOR signed_atom)*
 * signed_atom: [ADD_OPERATOR] atom
 * atom: number | function | variable | LEFT_PARANTHESIS expr RIGHT_PARENTHESIS
 * function: function_name LEFT_PARANTHESIS args RIGHT_PARENTHESIS
 * args: [expr (FUNC_ARG_SEPARATOR expr)*]
 * variable: identifier
 * identifier: alpha (alnum | special_symbols)*
 */

x3::rule<class expression, ast::Expression> const expression = "expression";
x3::rule<class addition, ast::BinaryExpression> const addition = "addition";
x3::rule<class multiplication, ast::BinaryExpression> const multiplication = "multiplication";
x3::rule<class exponentation, ast::ExponentationExpression> const exponentation = "exponentation";
x3::rule<class signed_atom, ast::UnaryExpression> const signed_atom = "signed_atom";
x3::rule<class atom, ast::Expression> const atom = "atom";
x3::rule<class function, ast::Function> const function = "function";
x3::rule<class arguments, std::vector<ast::Expression>> const arguments = "arguments";
x3::rule<class variable, ast::Variable> const variable = "variable";
x3::rule<class identifier, std::string> const identifier = "identifier";

auto const expression_def =
        addition;

auto const addition_def =
        multiplication >> *(ADD_OPERATOR > multiplication);

auto const multiplication_def =
        exponentation >> *(MULT_OPERATOR > exponentation);

auto const exponentation_def =
        signed_atom >> *(POW_OPERATOR > signed_atom);

auto const signed_atom_def =
        -ADD_OPERATOR >> atom;

auto const atom_def =
        number_without_sign |
        function |
        variable |
        (LEFT_PARANTHESIS > expression > RIGHT_PARENTHESIS);

auto const function_def =
        identifier >> LEFT_PARANTHESIS > arguments > RIGHT_PARENTHESIS;

auto const arguments_def =
        -(expression % FUNC_ARG_SEPARATOR);

auto const variable_def =
        identifier;

auto const identifier_def =
        x3::lexeme[x3::alpha >> *(x3::alnum | x3::char_('_'))];

BOOST_SPIRIT_DEFINE(
    expression,
    addition,
    multiplication,
    exponentation,
    signed_atom,
    atom,
    function,
    arguments,
    variable,
    identifier
);


} // namespace parser


namespace interpreter {


class FunctionEvaluator
{
public:

    static double evaluate(const std::string& name, const std::vector<double>& arguments)
    {
        if (name == "sin") {
            checkForOneArgument(name, arguments);
            return std::sin(arguments.at(0));
        }
        else if (name == "cos") {
            checkForOneArgument(name, arguments);
            return std::cos(arguments.at(0));
        }
        else if (name == "round") {
            checkForOneArgument(name, arguments);
            return std::round(arguments.at(0));
        }
        else if (name == "ceil") {
            checkForOneArgument(name, arguments);
            return std::ceil(arguments.at(0));
        }
        else if (name == "floor") {
            checkForOneArgument(name, arguments);
            return std::floor(arguments.at(0));
        }
        else if (name == "exp") {
            checkForOneArgument(name, arguments);
            return std::exp(arguments.at(0));
        }
        else if (name == "ln") {
            checkForOneArgument(name, arguments);
            return std::log(arguments.at(0));
        }
        else if (name == "sqrt") {
            checkForOneArgument(name, arguments);
            return std::sqrt(arguments.at(0));
        }
        else if (name == "pow") {
            checkForTwoArguments(name, arguments);
            return std::pow(arguments.at(0), arguments.at(1));
        }
        else if (name == "min") {
            checkForTwoOrMoreArguments(name, arguments);
            return *std::min_element(arguments.begin(), arguments.end());
        }
        else if (name == "max") {
            checkForTwoOrMoreArguments(name, arguments);
            return *std::max_element(arguments.begin(), arguments.end());
        }
        throwParseError(name, "unknown function");
        return 0.0;
    }

private:

    static void throwParseError(const std::string& name, const std::string& error)
    {
        throw calculator::ParseError("Function " + name + ": " + error + ".");
    }

    static void checkForZeroArguments(const std::string& name, const std::vector<double>& arguments)
    {
        if (arguments.size() != 0) {
            throwParseError(name, "expects zero argument(s)");
        }
    }

    static void checkForOneArgument(const std::string& name, const std::vector<double>& arguments)
    {
        if (arguments.size() != 1) {
            throwParseError(name, "expects one argument(s)");
        }
    }

    static void checkForTwoArguments(const std::string& name, const std::vector<double>& arguments)
    {
        if (arguments.size() != 2) {
            throwParseError(name, "expects two argument(s)");
        }
    }

    static void checkForTwoOrMoreArguments(const std::string& name, const std::vector<double>& arguments)
    {
        if (arguments.size() < 2) {
            throwParseError(name, "expects two or more argument(s)");
        }
    }

};

/*
 * Class that evaluates an abstract syntax tree and finds the number that the
 * tree represent.
 *
 * Use boost::apply_visitor on an ast::Operand to evaluate the ast tree. Throws
 * exceptions if it can't evaluate the tree.
 */
class Evaluator : public boost::static_visitor<double>
{
public:

    explicit Evaluator()
    {

    }

    explicit Evaluator(const std::map<std::string, double>& variables)
    {
        m_variables.insert(variables.begin(), variables.end());
    }

    double operator()(const double ast) const
    {
        return ast;
    }

    double operator()(const ast::UnaryExpression& ast) const
    {
        return sign(ast.command)*boost::apply_visitor(*this, ast.operand);
    }

    double operator()(const ast::BinaryExpression& ast) const
    {
        // Evaluate left to right for left associativity.
        double result = boost::apply_visitor(*this, ast.first);
        for (const ast::BinaryOperation& operation : ast.others) {
            const double second = boost::apply_visitor(*this, operation.operand);
            result = compute(operation.command, result, second);
        }
        return result;
    }

    double operator()(ast::ExponentationExpression ast) const
    {
        // Evaluate right to left for right associativity.
        // Leading signs are not part of base e.g -base^exp == -(base^exp).
        double exponent = 1.0;
        for (auto it = ast.others.crbegin(); it != ast.others.crend(); it++) {
            const double base = boost::apply_visitor(*this, it->operand);
            exponent = sign(it->command) * exponentiation(base, exponent);
        }
        const double base = boost::apply_visitor(*this, ast.first.operand);
        return sign(ast.first.command) * exponentiation(base, exponent);
    }

    double operator()(const ast::Function& ast) const
    {
        std::vector<double> arguments;

        for (const auto& argument : ast.arguments) {
            const double value = boost::apply_visitor(*this, argument);
            arguments.emplace_back(value);
        }

        return FunctionEvaluator::evaluate(ast.name, arguments);
    }

    double operator()(const ast::Variable& ast) const
    {
        const auto iter = m_variables.find(ast.name);
        if (iter != m_variables.end()) {
            return iter->second;
        }
        else {
            throw calculator::ParseError(ast.name + ": unknown variable");
        }
    }

private:

    static double sign(const boost::optional<char>& command)
    {
        if (command && command.get() == '-') {
            return -1.0;
        }
        else {
            return 1.0;
        }
    }

    static double division(const double a, const double b)
    {
        if (b == 0.0) {
            throw calculator::ParseError("division by zero");
        }
        return a / b;
    }

    static double remainder(const double a, const double b)
    {
        if (b == 0.0) {
            throw calculator::ParseError("division by zero in remainder calculation");
        }
        return std::fmod(a,b);
    }

    static double exponentiation(const double base, const double exponent)
    {
        return std::pow(base, exponent);
    }

    static double compute(char command, const double a, const double b)
    {
        if (command == '+') {
            return a + b;
        }
        else if (command == '-') {
            return a - b;
        }
        else if (command == '*') {
            return a * b;
        }
        else if (command == '/') {
            return division(a, b);
        }
        else if (command == '%') {
            return remainder(a, b);
        }
        assert(false);
        return 0.0;
    }

    std::map<std::string, double> m_variables = {
        {"pi", pi},
        {"e", euler},
    };

};

/*
 * Class that prints out an abstract syntax tree.
 *
 * Use boost::apply_visitor on an ast::Operand to print out the expression that
 * the operand represents.
 */
class Printer : public boost::static_visitor<void>
{
public:

    explicit Printer(std::ostream& out) : out(out)
    {

    }

    void operator()(const double ast) const
    {
        // The smallest unit in the ast tree: a number.
        out << ast;
    }

    void operator()(const ast::UnaryExpression& ast) const
    {
        // A + or - in front of an operand.
        out << "(";
        if (ast.command) {
            out << ast.command.get();
        }
        boost::apply_visitor(*this, ast.operand);
        out << ")";
    }

    void operator()(const ast::BinaryExpression& ast) const
    {
        // A binary operation (a+b+...+x) or (a*b*...*x) or etc.
        boost::apply_visitor(*this, ast.first);
        for (const ast::BinaryOperation& operation : ast.others) {
            out << operation.command;
            boost::apply_visitor(*this, operation.operand);
        }
    }

    void operator()(ast::ExponentationExpression ast) const
    {
        // An exponentation operation (a^b^...^x)
        out << "(";
        if (ast.first.command) {
            out << ast.first.command.get();
        }
        boost::apply_visitor(*this, ast.first.operand);
        out << ")";

        for (const auto& expression : ast.others) {
            out << "^";
            out << "(";
            if (expression.command) {
                out << expression.command.get();
            }
            boost::apply_visitor(*this, expression.operand);
            out << ")";
        }
    }

    void operator()(const ast::Function& ast) const
    {
        // A function call.
        out << ast.name << "(";

        bool isFirstElement = true;
        for (const ast::Expression& argument : ast.arguments) {
            if (isFirstElement) {
                isFirstElement = false;
                boost::apply_visitor(*this, argument);
            }
            else {
                out << ",";
                boost::apply_visitor(*this, argument);
            }
        }

        out << ")";
    }

    void operator()(const ast::Variable& ast) const
    {
        out << ast.name;
    }

private:

    std::ostream& out;

};


} // namespace interpreter


static void throwParseError(std::string::const_iterator error_location, const std::string& expression)
{
    std::string position;
    char character;
    if (error_location != expression.cend() && std::next(error_location) != expression.cend()) {
        position = "position "s + std::to_string((error_location - expression.cbegin()) + 1);
        character = *error_location;
    }
    else {
        position = "the end";
        character = expression.back();
    }
    throw calculator::ParseError("unexpected character '"s + character + "' at "s + position);
}

static void checkContainsOnlyAscii(const std::string& expression) 
{
    const auto is_non_ascii = [] (char character) { return character < 0; };
    const auto non_ascii = std::find_if(expression.cbegin(), expression.cend(), is_non_ascii);
    if (non_ascii != expression.cend()) {
        const auto position = std::to_string(non_ascii - expression.cbegin());
        throw calculator::ParseError("non-ascii character at position "s + position);
    }
}

static void checkIsNonEmpty(const std::string& expression)
{
    if (std::all_of(expression.cbegin(), expression.cend(), std::isspace)) {
        throw calculator::ParseError("expression is empty");
    }
}

static ast::Expression parseExpressionImpl(const std::string& expression)
{
    ast::Expression ast;

    try {
        auto parse_position = expression.cbegin();
        const bool parse_success = x3::phrase_parse(
            parse_position,
            expression.cend(),
            parser::expression,
            x3::space,
            ast
        );
        if (!parse_success) {
            throwParseError(parse_position, expression);
        }
        if (parse_position != expression.end()) {
            throwParseError(parse_position, expression);
        }
    }
    catch (const x3::expectation_failure<std::string::const_iterator>& error) {
        throwParseError(error.where(), expression);
    }

    return ast;
}

static ast::Expression parseExpression(const std::string& expression)
{
    checkContainsOnlyAscii(expression);
    checkIsNonEmpty(expression);
    return parseExpressionImpl(expression);
}

double calculator::evaluate(const std::string& expression, const std::map<std::string, double>& variables)
{
    ast::Expression ast = parseExpression(expression);
    interpreter::Evaluator evaluator(variables);
    return boost::apply_visitor(evaluator, ast);
}

std::string calculator::print(const std::string & expression)
{
    ast::Expression ast = parseExpression(expression);
    std::ostringstream out;
    interpreter::Printer printer(out);
    boost::apply_visitor(printer, ast);
    return out.str();
}

