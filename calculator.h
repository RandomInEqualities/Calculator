#pragma once

#include <map>
#include <stdexcept>
#include <string>

namespace calculator {

/**
 * The ParseError class is an exception thrown when a given expression cannot be parsed.
 *
 * The message will contain the error location in the expression.
 */
class ParseError : public std::runtime_error
{
    using std::runtime_error::runtime_error;
};

/**
 * Evaluate an expression and return the result that it represents.
 */
double evaluate(const std::string& expression, const std::map<std::string, double>& variables = {});

/**
 * Convert an expression into an abstract syntax tree and back again to a string. 
 *
 * Tells how the parser parses the expression.
 */
std::string print(const std::string& expression);

} // namespace calculator

