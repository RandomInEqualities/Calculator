#include "calculator.h"

#include <iostream>
#include <stdexcept>
#include <string>

int main()
{
    std::string expression;

    while (true) {

        std::cout << "Type Expression To Evaluate: ";
        std::getline(std::cin, expression);

        if (expression == "quit") {
            return 0;
        }

        try {
            std::string ast = calculator::print(expression);
            std::cout << "Ast is: " << ast << std::endl;
            double result = calculator::evaluate(expression);
            std::cout << "Result is: " << result << std::endl;
        }
        catch (const calculator::ParseError& error) {
            std::cout << "Could not evaluate: " << error.what() << std::endl;
        }

    }
}
