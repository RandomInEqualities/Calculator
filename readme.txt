# About

    Calculator that evaluates mathematical expressions. 
    Supports variables and functions.

# Requires

    Boost Spirit X3 (its a header only library in boost)

# X3 docs

    https://www.boost.org/doc/libs/1_70_0/libs/spirit/doc/x3/html/index.htmls

